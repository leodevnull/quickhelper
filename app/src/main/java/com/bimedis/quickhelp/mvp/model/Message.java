package com.bimedis.quickhelp.mvp.model;


import android.os.Parcel;
import android.os.Parcelable;

import java.util.HashMap;
import java.util.Map;

public class Message implements Parcelable{
    private static final String MAP_TITLE = "title";
    public static final String MAP_DATE = "date";
    private static final String MAP_TEXT = "text";
    private static final String MAP_FILES = "file";
    private static final String MAP_TYPE = "type";

    private String title;
    private long date;
    private String text;
    private String file;
    private int type;

    public Message(){}

    protected Message(Parcel in) {
        title = in.readString();
        date = in.readLong();
        text = in.readString();
        file = in.readString();
        type = in.readInt();
    }

    public static final Creator<Message> CREATOR = new Creator<Message>() {
        @Override
        public Message createFromParcel(Parcel in) {
            return new Message(in);
        }

        @Override
        public Message[] newArray(int size) {
            return new Message[size];
        }
    };

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public Map<String, Object> getMap(){
        Map<String, Object> map = new HashMap<>();
        map.put(MAP_TITLE, getTitle());
        map.put(MAP_DATE, getDate());
        map.put(MAP_TEXT, getText());
        map.put(MAP_FILES, getFile());
        map.put(MAP_TYPE, getType());
        return map;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeLong(date);
        dest.writeString(text);
        dest.writeString(file);
        dest.writeInt(type);
    }
}
