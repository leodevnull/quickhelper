package com.bimedis.quickhelp.mvp.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.HashMap;
import java.util.Map;

public class User implements Parcelable{
    public static final String MAP_TOKEN = "token";
    private static final String MAP_NAME = "name";
    private static final String MAP_PHONE_1 = "phone1";
    private static final String MAP_PHONE_2 = "phone2";
    private static final String MAP_EMAIL = "email";
    public static final String MAP_DEVICE_TOKEN = "device_token";
    private static final String MAP_MODEL = "model";

    private String token;
    private String name;
    private String phone1;
    private String phone2;
    private String email;
    private String device_token;
    private String model;

    public User(){}

    protected User(Parcel in) {
        token = in.readString();
        name = in.readString();
        phone1 = in.readString();
        phone2 = in.readString();
        email = in.readString();
        device_token = in.readString();
        model = in.readString();
    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone1() {
        return phone1;
    }

    public void setPhone1(String phone1) {
        this.phone1 = phone1;
    }

    public String getPhone2() {
        return phone2;
    }

    public void setPhone2(String phone2) {
        this.phone2 = phone2;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDevice_token() {
        return device_token;
    }

    public void setDevice_token(String device_token) {
        this.device_token = device_token;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(token);
        dest.writeString(name);
        dest.writeString(phone1);
        dest.writeString(phone2);
        dest.writeString(email);
        dest.writeString(device_token);
        dest.writeString(model);
    }

    public Map<String, Object> getMap(){
        Map<String, Object> map = new HashMap<>();
        map.put(MAP_TOKEN, getToken());
        map.put(MAP_NAME, getName());
        map.put(MAP_PHONE_1, getPhone1());
        map.put(MAP_PHONE_2, getPhone2());
        map.put(MAP_EMAIL, getEmail());
        map.put(MAP_DEVICE_TOKEN, getDevice_token());
        map.put(MAP_MODEL, getModel());
        return map;
    }
}
