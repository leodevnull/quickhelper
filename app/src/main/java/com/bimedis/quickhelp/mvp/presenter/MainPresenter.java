package com.bimedis.quickhelp.mvp.presenter;


import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.bimedis.quickhelp.BuildConfig;
import com.bimedis.quickhelp.Constants;
import com.bimedis.quickhelp.R;
import com.bimedis.quickhelp.mvp.model.User;
import com.bimedis.quickhelp.mvp.view.MainView;
import com.bimedis.quickhelp.utils.FirebaseHelper;
import com.bimedis.quickhelp.utils.Utils;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;
import com.vicmikhailau.maskededittext.MaskedFormatter;

import java.util.Locale;

@InjectViewState
public class MainPresenter extends MvpPresenter<MainView> {
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    private FirebaseRemoteConfig mRemoteConfig;

    public void checkUserOnStart(){
        if (mAuth.getCurrentUser() == null){
            getViewState().showDialog(R.string.progress_dialog_title, true);
            mAuth.signInAnonymously()
                    .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()){
                                createOrUpdateUser();
                            }
                        }
                    });
        }
    }

    public void startIntent(final int intentCode){
        if (mAuth.getCurrentUser() == null){
            getViewState().showDialog(R.string.progress_dialog_title, true);
            mAuth.signInAnonymously()
                    .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()){
                                createOrUpdateUser();
                                getViewState().startIntent(intentCode);
                            }
                        }
                    });
        } else {
            getViewState().startIntent(intentCode);
        }
    }

    private void createOrUpdateUser() {
        String token = "";
        if (mAuth.getCurrentUser() != null){
            token = mAuth.getCurrentUser().getUid();
        }
        User user = new User();
        user.setToken(token);
        user.setModel(Utils.getDeviceModel());
        user.setDevice_token(Utils.getDeviceId());
        FirebaseHelper.createOrUpdateUser(user);
        getViewState().saveNewTokens(user.getToken(), user.getDevice_token());
        getViewState().hideDialog();
    }

    public void requestCall(String simLanguage, final String phone, String hint) {
        if (!TextUtils.isEmpty(phone) && phone.length() == hint.length()){
            getViewState().showDialog(R.string.leaving_request, false);
            getViewState().saveUserPhone(phone);
            FirebaseHelper.newRequestCallTask(simLanguage, phone).addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    FirebaseHelper.updateUserPhone(phone);
                    getViewState().showMessage(R.string.request_done);
                    getViewState().hideDialog();
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    getViewState().showMessage(R.string.request_call_error);
                }
            });
        } else {
            getViewState().showMessage(R.string.req_empty);
        }
    }

    public void initRemoteConfig(){
        FirebaseRemoteConfigSettings configSettings = new FirebaseRemoteConfigSettings.Builder()
                .setDeveloperModeEnabled(BuildConfig.DEBUG)
                .build();

        mRemoteConfig = FirebaseRemoteConfig.getInstance();
        mRemoteConfig.setConfigSettings(configSettings);
        mRemoteConfig.setDefaults(R.xml.firebase_default_config);

        mRemoteConfig.fetch(0)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()){
                            mRemoteConfig.activateFetched();
                        }
                        initManagersTitle();
                    }
                });
    }

    public void updateLanguage(int lang){
        String hint;
        MaskedFormatter formatter;
        if (lang == Constants.LANG_UA){
            formatter = new MaskedFormatter(Constants.MASK_UA);
            hint = Constants.HINT_UA;
        }else {
            formatter = new MaskedFormatter(Constants.MASK_RU);
            hint = Constants.HINT_RU;
        }
        getViewState().updateMaskedEditText(formatter, hint);
    }

    private void initManagersTitle(){
        Locale locale = Locale.getDefault();
        String managerLabel1, managerLabel2;
        if (locale.getLanguage().equals(new Locale(Constants.ISO_LANG_UA).getLanguage())){
            managerLabel1 = mRemoteConfig.getString(Constants.MANAGER_LABEL_UA_1);
            managerLabel2 = mRemoteConfig.getString(Constants.MANAGER_LABEL_UA_2);
        } else if (locale.getLanguage().equals(new Locale(Constants.ISO_LANG_RU).getLanguage())){
            managerLabel1 = mRemoteConfig.getString(Constants.MANAGER_LABEL_RU_1);
            managerLabel2 = mRemoteConfig.getString(Constants.MANAGER_LABEL_RU_2);
        } else {
            managerLabel1 = mRemoteConfig.getString(Constants.MANAGER_LABEL_EN_1);
            managerLabel2 = mRemoteConfig.getString(Constants.MANAGER_LABEL_EN_2);
        }
        getViewState().initManagersTitle(managerLabel1, managerLabel2);
    }

}
