package com.bimedis.quickhelp.mvp.view;


import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;
import com.vicmikhailau.maskededittext.MaskedFormatter;

@StateStrategyType(AddToEndSingleStrategy.class)
public interface MainView extends BaseView {
    void initManagersTitle(String title1, String title2);

    void saveNewTokens(String token, String device_token);
    void startIntent(int intentCode);
    void saveUserPhone(String phone);
    void showMessage(int messageIdRes);
    void updateMaskedEditText(MaskedFormatter formatter, String hint);
}
