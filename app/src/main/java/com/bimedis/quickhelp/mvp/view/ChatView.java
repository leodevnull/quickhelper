package com.bimedis.quickhelp.mvp.view;


import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;
import com.bimedis.quickhelp.mvp.model.Message;
import com.google.firebase.database.Query;

import java.io.File;

@StateStrategyType(AddToEndSingleStrategy.class)
public interface ChatView extends BaseView {
    void setDataToAdapter(Query query);
    void addImageToGallery(String filePath);
    void startCameraIntent(File imageFile, int requestCamera);
    void showImagePreview(String filePath);
    void clearInputs();
    void setVisiblePreviewImage(boolean isVisible);
    void startFilePickerIntent(int requestCode);
    void startMessageService(String deviceToken, String chatTitle, Message message);
}
