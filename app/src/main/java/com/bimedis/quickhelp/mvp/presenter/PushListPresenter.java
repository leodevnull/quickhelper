package com.bimedis.quickhelp.mvp.presenter;

import android.view.View;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.bimedis.quickhelp.R;
import com.bimedis.quickhelp.adapter.PushViewHolder;
import com.bimedis.quickhelp.mvp.model.Push;
import com.bimedis.quickhelp.mvp.view.PushListView;
import com.bimedis.quickhelp.utils.FirebaseHelper;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

@InjectViewState
public class PushListPresenter extends MvpPresenter<PushListView> {
    public void onCreate(String deviceToken){
        getViewState().initToolbar();
        initRecycler(deviceToken);
    }

    private void initRecycler(final String deviceToken) {
        getViewState().showDialog(R.string.load_data, false);
        Query query = FirebaseHelper.getPushes();
        FirebaseRecyclerAdapter<Push, PushViewHolder> adapter =
                new FirebaseRecyclerAdapter<Push, PushViewHolder>
                        (Push.class, R.layout.push_list_item, PushViewHolder.class, query) {
                    @Override
                    protected void populateViewHolder(final PushViewHolder viewHolder, final Push model, final int position) {
                        final Query query = FirebaseHelper.getVoting(getRef(position));
                        showLikeOnPushes(query, deviceToken, viewHolder, model);

                        viewHolder.itemView.findViewById(R.id.push_item_like)
                                .setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        query.addListenerForSingleValueEvent(new ValueEventListener() {
                                            @Override
                                            public void onDataChange(DataSnapshot dataSnapshot) {
                                                onClickLike(dataSnapshot, deviceToken);
                                            }

                                            @Override
                                            public void onCancelled(DatabaseError databaseError) {
                                            }
                                        });
                                    }
                                });
                        viewHolder.itemView.findViewById(R.id.push_item_button)
                                .setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        getViewState().startChatWithPush(model.getTitle());
                                    }
                                });
                        viewHolder.bind(model);
                    }
                };
        onDataLoadComplete(query);
        getViewState().setupRecycler(adapter);
    }

    private void onDataLoadComplete(Query query) {
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                getViewState().hideDialog();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                getViewState().hideDialog();
            }
        });
    }

    private void showLikeOnPushes(Query query, final String token, final PushViewHolder viewHolder, final Push model) {
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot x : dataSnapshot.getChildren()){
                    if (x.getValue().equals(token)){
                        viewHolder.toggleLike(true);
                        model.setLike(true);
                    }
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {}
        });
    }

    private void onClickLike(DataSnapshot dataSnapshot, String token) {
        if (dataSnapshot.getValue() == null){
            dataSnapshot.child("0").getRef().setValue(token);
        } else {
            for (DataSnapshot x : dataSnapshot.getChildren()){
                if (x.getValue().equals(token)){
                    dataSnapshot.child(x.getKey()).getRef().removeValue();
                    break;
                } else {
                    int element = Integer.parseInt(x.getKey()) + 1;
                    dataSnapshot.child(String.valueOf(element)).getRef().setValue(token);
                }
            }
        }
    }
}
