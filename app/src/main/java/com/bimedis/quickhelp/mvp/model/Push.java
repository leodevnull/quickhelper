package com.bimedis.quickhelp.mvp.model;


public class Push {
    private int uid;
    private String title;
    private boolean isLike;

    public Push(){}

    public Push(String title) {
        this.title = title;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isLike() {
        return isLike;
    }

    public void setLike(boolean like) {
        isLike = like;
    }

    public void toggleLike(){
        isLike = !isLike;
    }
}
