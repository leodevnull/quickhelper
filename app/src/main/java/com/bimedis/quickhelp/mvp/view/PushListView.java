package com.bimedis.quickhelp.mvp.view;

import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;
import com.firebase.ui.database.FirebaseRecyclerAdapter;

@StateStrategyType(AddToEndSingleStrategy.class)
public interface PushListView extends BaseView {
    void initToolbar();
    void setupRecycler(FirebaseRecyclerAdapter adapter);
    @StateStrategyType(SkipStrategy.class)
    void startChatWithPush(String pushTitle);
}
