package com.bimedis.quickhelp.mvp.presenter;


import android.net.Uri;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.bimedis.quickhelp.R;
import com.bimedis.quickhelp.mvp.model.Message;
import com.bimedis.quickhelp.mvp.view.ChatView;
import com.bimedis.quickhelp.utils.FirebaseHelper;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.UploadTask;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

@InjectViewState
public class ChatPresenter extends MvpPresenter<ChatView>{
    private static final int REQUEST_CAMERA = 1000;
    private static final int REQUEST_FILES = 1001;
    private String deviceToken;
    private String chatTitle;
    private String filePath;

    public void onCreate(String deviceToken, String chatTitle){
        this.deviceToken = deviceToken;
        this.chatTitle = chatTitle;
        getViewState().showDialog(R.string.load_data, false);
        Query query = FirebaseHelper.getMessages(deviceToken, chatTitle);
        onDataLoad(query);
        getViewState().setDataToAdapter(query);
    }

    private void onDataLoad(Query query) {
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                getViewState().hideDialog();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                getViewState().hideDialog();
            }
        });
    }

    public void onActivityResult(int requestCode, @Nullable Uri data) {
        if (requestCode == REQUEST_FILES) {
            if (data != null) {
                filePath = data.toString();
                getViewState().setVisiblePreviewImage(true);
                getViewState().showImagePreview(filePath);
            }
        } else if (requestCode == REQUEST_CAMERA){
            getViewState().addImageToGallery(filePath);
            filePath = Uri.fromFile(new File(filePath)).toString();
            getViewState().setVisiblePreviewImage(true);
            getViewState().showImagePreview(filePath);
        }
    }

    @SuppressWarnings("VisibleForTests")
    public void sendMessage(String title, String text){
        if (TextUtils.isEmpty(text)){
            return;
        }
        final Message message = new Message();
        message.setTitle(title);
        message.setDate(new Date().getTime());
        message.setText(text);
        if (!TextUtils.isEmpty(filePath)){
            getViewState().setVisiblePreviewImage(false);
            FirebaseHelper.uploadImage(deviceToken, filePath)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            message.setFile(taskSnapshot.getDownloadUrl().toString());
                            filePath = null;
                            getViewState().startMessageService(deviceToken, chatTitle, message);
                        }
                    });
        } else {
            getViewState().startMessageService(deviceToken, chatTitle, message);
        }
        getViewState().clearInputs();
    }

    public void onCameraClick(){
        getViewState().startCameraIntent(createImageFile(), REQUEST_CAMERA);
    }

    public void onFilePickerClick(){
        getViewState().startFilePickerIntent(REQUEST_FILES);
    }

    private File createImageFile() {
        String timeStamp = new SimpleDateFormat("dd.MM.yy_HH.mm.ss", Locale.US).format(new Date());
        String imageFileName = "QH_" + timeStamp;
        File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES + "/RH");
        if (!storageDir.exists())
            storageDir.mkdir();
        File image = new File(storageDir, imageFileName + ".jpg");
        filePath = image.getAbsolutePath();
        return image;
    }

    public void onActivityResultCancel() {
        filePath = null;
    }

    public void onPreviewCancelClick(){
        filePath = null;
        getViewState().setVisiblePreviewImage(false);
    }
}
