package com.bimedis.quickhelp.expand;


import android.content.Context;

import com.bimedis.quickhelp.R;
import com.bimedis.quickhelp.expand.model.SocialGroup;
import com.bimedis.quickhelp.expand.model.Socials;
import com.bimedis.quickhelp.utils.SocialsHelper;

import java.util.ArrayList;
import java.util.List;
public class SocialBuilder {

    public static List<SocialGroup> getSocialGroup(Context context, String title){
        List<SocialGroup> socialGroups = new ArrayList<>();
        socialGroups.add(new SocialGroup(title, getSocials(context), R.drawable.ic_chat));
        return socialGroups;
    }

    private static List<Socials> getSocials(Context context){
        List<Socials> socialsList = new ArrayList<>();
        socialsList.add(new Socials(R.drawable.skype_chat, R.string.skype_chat, SocialsHelper.getSkypeUri(context)));
        socialsList.add(new Socials(R.drawable.viber_chat, R.string.viber_chat, SocialsHelper.getViberUri(context)));
        socialsList.add(new Socials(R.drawable.vk_chat, R.string.vk_chat, SocialsHelper.getVkUri()));
        socialsList.add(new Socials(R.drawable.telegram_chat, R.string.telegram_chat, SocialsHelper.getTelegramUri(context)));
        socialsList.add(new Socials(R.drawable.linkedin_chat, R.string.link_chat, SocialsHelper.getLinkedinUri()));
        socialsList.add(new Socials(R.drawable.google_chat, R.string.google_chat, SocialsHelper.getGoogleUri()));
        socialsList.add(new Socials(R.drawable.fecebook_chat, R.string.fb_chat, SocialsHelper.getFacebookUri(context)));
        socialsList.add(new Socials(R.drawable.ok_chat, R.string.ok_chat, SocialsHelper.getOkUri()));
        return socialsList;
    }

}
