package com.bimedis.quickhelp.expand.model;

import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;

import java.util.List;


public class SocialGroup extends ExpandableGroup<Socials> {
    private int iconResId;

    public SocialGroup(String title, List<Socials> items, int iconResId) {
        super(title, items);
        this.iconResId = iconResId;
    }

    public int getIconResId() {
        return iconResId;
    }
}
