package com.bimedis.quickhelp.expand.holder;

import android.graphics.Color;
import android.view.View;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.TextView;

import com.bimedis.quickhelp.R;
import com.bimedis.quickhelp.expand.model.SocialGroup;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;
import com.thoughtbot.expandablerecyclerview.viewholders.GroupViewHolder;

import static android.view.animation.Animation.RELATIVE_TO_SELF;


public class SocialGroupHolder extends GroupViewHolder {
    private ImageView mGroupImage;
    private TextView mGroupTitle;
    private ImageView mArrow;


    public SocialGroupHolder(View itemView) {
        super(itemView);
        mGroupImage = (ImageView) itemView.findViewById(R.id.expand_group_image);
        mGroupTitle = (TextView) itemView.findViewById(R.id.expand_group_title);
        mArrow = (ImageView) itemView.findViewById(R.id.expand_group_arrow);
    }

    public void setGroup(ExpandableGroup group){
        mGroupImage.setImageResource(((SocialGroup)group).getIconResId());
        mGroupTitle.setText(group.getTitle());
    }

    @Override
    public void expand() {
        mArrow.setColorFilter(Color.GRAY);
        animateExpand();
    }

    @Override
    public void collapse() {
        mArrow.setColorFilter(Color.RED);
        animateCollapse();
    }

    private void animateExpand() {
        RotateAnimation rotate =
                new RotateAnimation(360, 180, RELATIVE_TO_SELF, 0.5f, RELATIVE_TO_SELF, 0.5f);
        rotate.setDuration(300);
        rotate.setFillAfter(true);
        mArrow.setAnimation(rotate);
    }

    private void animateCollapse() {
        RotateAnimation rotate =
                new RotateAnimation(180, 360, RELATIVE_TO_SELF, 0.5f, RELATIVE_TO_SELF, 0.5f);
        rotate.setDuration(300);
        rotate.setFillAfter(true);
        mArrow.setAnimation(rotate);
    }
}
