package com.bimedis.quickhelp.expand.holder;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bimedis.quickhelp.R;
import com.bimedis.quickhelp.expand.SocialGroupAdapter;
import com.bimedis.quickhelp.expand.model.Socials;
import com.thoughtbot.expandablerecyclerview.viewholders.ChildViewHolder;


public class SocialChildHolder extends ChildViewHolder {
    private ImageView mSocialImage;
    private TextView mSocialTitle;
    private ImageView mArrow;

    public SocialChildHolder(View itemView) {
        super(itemView);
        mSocialImage = (ImageView) itemView.findViewById(R.id.expand_item_image);
        mSocialTitle = (TextView) itemView.findViewById(R.id.expand_item_title);
        mArrow = (ImageView) itemView.findViewById(R.id.expand_item_arrow);
    }

    public void onBind(final Socials socials, final SocialGroupAdapter.OnSocialClickListener listener){
        mSocialImage.getRootView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(socials);
            }
        });
        mSocialImage.setImageResource(socials.getImageResource());
        mSocialTitle.setText(socials.getTitle());

    }

}
