package com.bimedis.quickhelp.expand.model;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;


public class Socials implements Parcelable {
    private int mImageResource;
    private int mTitleResource;
    private Uri mSocialUri;

    public Socials(int imageResource, int title, Uri socialUri) {
        mImageResource = imageResource;
        mTitleResource = title;
        mSocialUri = socialUri;
    }

    protected Socials(Parcel in) {
        mImageResource = in.readInt();
        mTitleResource = in.readInt();
        mSocialUri = in.readParcelable(Uri.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(mImageResource);
        dest.writeInt(mTitleResource);
        dest.writeParcelable(mSocialUri, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Socials> CREATOR = new Creator<Socials>() {
        @Override
        public Socials createFromParcel(Parcel in) {
            return new Socials(in);
        }

        @Override
        public Socials[] newArray(int size) {
            return new Socials[size];
        }
    };

    public Uri getSocialUri() {
        return mSocialUri;
    }

    public void setSocialUri(Uri socialUri) {
        mSocialUri = socialUri;
    }

    public int getImageResource() {
        return mImageResource;
    }

    public void setImageResource(int imageResource) {
        mImageResource = imageResource;
    }

    public int getTitle() {
        return mTitleResource;
    }

    public void setTitle(int title) {
        mTitleResource = title;
    }
}
