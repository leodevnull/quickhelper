package com.bimedis.quickhelp.expand;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bimedis.quickhelp.R;
import com.bimedis.quickhelp.expand.holder.SocialGroupHolder;
import com.bimedis.quickhelp.expand.holder.SocialChildHolder;
import com.bimedis.quickhelp.expand.model.SocialGroup;
import com.bimedis.quickhelp.expand.model.Socials;
import com.thoughtbot.expandablerecyclerview.ExpandableRecyclerViewAdapter;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;

import java.util.List;


public class SocialGroupAdapter extends ExpandableRecyclerViewAdapter<SocialGroupHolder, SocialChildHolder> {
    private OnSocialClickListener mOnSocialClickListener;

    public SocialGroupAdapter(List<? extends ExpandableGroup> groups, OnSocialClickListener listener) {
        super(groups);
        mOnSocialClickListener = listener;
    }

    public interface OnSocialClickListener{
        void onItemClick(Socials socials);
    }

    @Override
    public SocialGroupHolder onCreateGroupViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.expand_list_group, parent, false);
        return new SocialGroupHolder(view);
    }

    @Override
    public SocialChildHolder onCreateChildViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.expand_list_item, parent, false);
        return new SocialChildHolder(view);
    }

    @Override
    public void onBindChildViewHolder(SocialChildHolder holder, int flatPosition, ExpandableGroup group, int childIndex) {
        Socials socials = ((SocialGroup)group).getItems().get(childIndex);
        holder.onBind(socials, mOnSocialClickListener);
    }

    @Override
    public void onBindGroupViewHolder(SocialGroupHolder holder, int flatPosition, ExpandableGroup group) {
        holder.setGroup(group);
    }
}
