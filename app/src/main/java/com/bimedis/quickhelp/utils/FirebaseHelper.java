package com.bimedis.quickhelp.utils;


import android.net.Uri;

import com.bimedis.quickhelp.mvp.model.Message;
import com.bimedis.quickhelp.mvp.model.User;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class FirebaseHelper {
    private static final String PUSH_CHILD = "pushes";
    private static final String PUSH_UID = "uid";
    private static final String USER_CHILD = "users";
    private static final String VOTING_CHILD = "voting";
    private static final String CHAT_CHILD = "chat";
    private static final String REQUEST_CALL_CHILD = "request_call";
    private static final String FB_TOKEN = "fb_token";

    private static FirebaseDatabase sDatabase;

    public static UploadTask uploadImage(String deviceToken, String filePath){
        String fileName = Utils.generateUploadFileName();
        FirebaseStorage firebaseStorage = FirebaseStorage.getInstance();
        StorageReference reference = firebaseStorage.getReference()
                .child(deviceToken)
                .child(fileName);
        return reference.putFile(Uri.parse(filePath));
    }

    public static void sendMessage(String deviceToken, String chatChildTitle, Message message){
        sDatabase = FirebaseDatabase.getInstance();
        sDatabase.getReference().child(CHAT_CHILD)
                .child(deviceToken)
                .child(chatChildTitle)
                .push()
                .updateChildren(message.getMap());
    }

    public static void createTestPushes(){
        sDatabase = FirebaseDatabase.getInstance();
        Map<String, Object> map;
        for (int i = 0; i < 10; i++){
            map = new HashMap<>();
            map.put("uid", i);
            map.put("date", new Date().getTime());
            map.put("title", "title " + i);
            map.put("message", "message long " + i);
            sDatabase.getReference().child(PUSH_CHILD).push().updateChildren(map);
        }
    }

    public static Query getVoting(DatabaseReference votingParent){
        return votingParent.child(VOTING_CHILD);
    }

    public static Query getPushes(){
        sDatabase = FirebaseDatabase.getInstance();
        return sDatabase.getReference().child(PUSH_CHILD);
    }

    public static Query getMessages(String deviceToken, String pushTitle){
        sDatabase = FirebaseDatabase.getInstance();
        return sDatabase.getReference()
                .child(CHAT_CHILD)
                .child(deviceToken)
                .child(pushTitle).orderByChild(Message.MAP_DATE);
    }

    public static Query getPushTitleById(int pushUid){
        sDatabase = FirebaseDatabase.getInstance();
        return sDatabase.getReference()
                .child(PUSH_CHILD)
                .orderByChild(PUSH_UID)
                .equalTo(pushUid);
    }

    public static void createOrUpdateUser(final User user){
        sDatabase = FirebaseDatabase.getInstance();
        sDatabase.getReference().child(USER_CHILD)
                .orderByChild(User.MAP_DEVICE_TOKEN)
                .equalTo(Utils.getDeviceId())
                .addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue() != null){
                    String key = dataSnapshot.getChildren().iterator().next().getKey();
                    dataSnapshot.getRef().child(key).child(User.MAP_TOKEN).setValue(user.getToken());
                }else {
                    addUser(user);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {}
        });
    }

    public static Task<Void> newRequestCallTask(String simLanguage, String phone){
        Map<String, Object> map = new HashMap<>();
        map.put("date", new Date().getTime());
        map.put("phone", phone);
        map.put("interface_lang", Locale.getDefault().getISO3Language());
        map.put("sim_lang", simLanguage);
        sDatabase = FirebaseDatabase.getInstance();
        return sDatabase.getReference().child(REQUEST_CALL_CHILD)
                .child(Utils.getDeviceId())
                .push()
                .updateChildren(map);
    }

    public static void updateUserPhone(final String phone){
        sDatabase = FirebaseDatabase.getInstance();
        sDatabase.getReference().child(USER_CHILD)
                .orderByChild(User.MAP_DEVICE_TOKEN)
                .equalTo(Utils.getDeviceId())
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        for (DataSnapshot x : dataSnapshot.getChildren()){
                            x.getRef().child("phone1").setValue(phone);
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }

    public static void updateFbToken() {
        FirebaseDatabase.getInstance().getReference().child(USER_CHILD).orderByChild(User.MAP_DEVICE_TOKEN)
                .equalTo(Utils.getDeviceId())
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (dataSnapshot.getValue() != null){
                            String key = dataSnapshot.getChildren().iterator().next().getKey();
                            dataSnapshot.getRef().child(key).child(FB_TOKEN).setValue(FirebaseInstanceId.getInstance().getToken());
                            dataSnapshot.getRef().removeEventListener(this);
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                    }
                });
    }

    private static void addUser(User user){
        sDatabase = FirebaseDatabase.getInstance();
        sDatabase.getReference().child(USER_CHILD).push().updateChildren(user.getMap());
    }
}
