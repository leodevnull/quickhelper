package com.bimedis.quickhelp.utils;


import android.content.Context;
import android.net.Uri;

import com.bimedis.quickhelp.Constants;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;

public class SocialsHelper {
    public static Uri getSkypeUri(Context context){
        return Utils.isAppInstalled(context, Constants.SKYPE_PACKAGE) ?
                Uri.parse("skype:"
                        + FirebaseRemoteConfig.getInstance().getString(Constants.SKYPE_CHAT)
                        + "?chat")
                : Utils.gotoMarket(Constants.SKYPE_PACKAGE);
    }

    public static Uri getVkUri(){
        return Uri.parse("https://vk.com/"
                + FirebaseRemoteConfig.getInstance().getString(Constants.VK_CHAT));
    }

    public static Uri getViberUri(Context context){
        return Utils.isAppInstalled(context, Constants.VIBER_PACKAGE) ?
                Uri.parse("tel:"
                        + Uri.encode(FirebaseRemoteConfig.getInstance().getString(Constants.VIBER_CHAT)))
                : Utils.gotoMarket(Constants.VIBER_PACKAGE);
    }

    public static Uri getTelegramUri(Context context){
        return Utils.isAppInstalled(context, Constants.TELEGRAM_PACKAGE) ?
                Uri.parse("https://telegram.me/"
                + FirebaseRemoteConfig.getInstance().getString(Constants.TELTGRAM_CHAT))
                : Utils.gotoMarket(Constants.TELEGRAM_PACKAGE);
    }

    public static Uri getLinkedinUri(){
        return Uri.parse(FirebaseRemoteConfig.getInstance().getString(Constants.LINKEDIN_CHAT));
    }

    public static Uri getGoogleUri(){
        return Uri.parse(FirebaseRemoteConfig.getInstance().getString(Constants.GOOGLE_CHAT));
    }

    public static Uri getFacebookUri(Context context){
        return Utils.isAppInstalled(context, Constants.FB_MESSENGER_PACKAGE) ?
                Uri.parse("fb-messenger://user/"
                        + FirebaseRemoteConfig.getInstance().getString(Constants.FB_CHAT))
                : Utils.gotoMarket(Constants.FB_MESSENGER_PACKAGE);
/*                : Uri.parse("https://www.facebook.com/messages/t/"
                + FirebaseRemoteConfig.getInstance().getString(Constants.FB_CHAT));*/
    }

    public static Uri getOkUri(){
        return Uri.parse("https://ok.ru/"
                + FirebaseRemoteConfig.getInstance().getString(Constants.OK_CHAT));
    }
}
