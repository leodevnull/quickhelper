package com.bimedis.quickhelp.utils;


import android.content.Context;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.telephony.TelephonyManager;

import com.bimedis.quickhelp.Constants;
import com.bimedis.quickhelp.global.SPManager;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Utils {
    private static SimpleDateFormat sFormat = new SimpleDateFormat("dd.MM.HH.mm", Locale.US);

    public static String generateUploadFileName(){
        return "QH_" + sFormat.format(new Date());
    }

    public static String getDeviceId(){
        return Build.ID;
    }

    public static String getDeviceModel() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return capitalize(model);
        } else {
            return capitalize(manufacturer) + " " + model;
        }
    }

    private static String capitalize(String s) {
        if (s == null || s.length() == 0) {
            return "";
        }
        char first = s.charAt(0);
        if (Character.isUpperCase(first)) {
            return s;
        } else {
            return Character.toUpperCase(first) + s.substring(1);
        }
    }

    public static boolean isAppInstalled(Context context, String packageName){
        PackageManager manager = context.getPackageManager();
        try{
            manager.getPackageInfo(packageName, PackageManager.GET_ACTIVITIES);
        } catch (PackageManager.NameNotFoundException e){
            return false;
        }
        return true;
    }

    public static Uri gotoMarket(String packageName){
        return Uri.parse("market://details?id=" + packageName);
    }

    public static boolean isOnline(Context context) {
        ConnectivityManager cm = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    public static int getLanguageCodeBySim(Context context){
        int languageCode = Constants.LANG_RU;
        TelephonyManager tm = (TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);
        String countryCode = tm.getSimCountryIso();
        if (countryCode.equalsIgnoreCase("ua")){
            languageCode = Constants.LANG_UA;
        }
        SPManager.setSimLanguage(context, countryCode);
        return languageCode;
    }
}
