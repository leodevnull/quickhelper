package com.bimedis.quickhelp.utils;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import com.bimedis.quickhelp.Constants;
import com.bimedis.quickhelp.ui.activity.ChatActivity;
import com.bimedis.quickhelp.ui.activity.PushListActivity;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;

public class IntentHelper {
    private static Intent sIntent;

    public static Intent getFbLinkIntent(Context context){
        Uri uri = Utils.isAppInstalled(context, Constants.FB_PACKAGE) ?
                Uri.parse("fb://facewebmodal/f?href=https://www.facebook.com/profile.php?id="
                        + FirebaseRemoteConfig.getInstance().getString(Constants.FB_LINK))
                : Uri.parse("https://www.facebook.com/"
                + FirebaseRemoteConfig.getInstance().getString(Constants.FB_LINK));
        return getSocialChatIntent(uri);
    }

    public static Intent getVkIntent(){
        return getSocialChatIntent(Uri.parse("https://vk.com/"
                + FirebaseRemoteConfig.getInstance().getString(Constants.VK_LINK)));
    }

    public static Intent getOkIntent(){
        return getSocialChatIntent(Uri.parse("https://ok.ru/"
                + FirebaseRemoteConfig.getInstance().getString(Constants.OK_LINK)));
    }

    public static Intent getTwitIntent(){
        return getSocialChatIntent(Uri.parse("https://twitter.com/"
                + FirebaseRemoteConfig.getInstance().getString(Constants.TWIT_LINK)));
    }

    public static Intent getGoogleIntent(){
        return getSocialChatIntent(Uri.parse(FirebaseRemoteConfig.getInstance().getString(Constants.GOOGLE_LINK)));
    }

    public static Intent getManagerIntent1(){
        return sIntent = new Intent(
                Intent.ACTION_DIAL,
                Uri.parse("tel:" + Uri.encode(
                        FirebaseRemoteConfig
                                .getInstance()
                                .getString(Constants.MANAGER_PHONE_1))));
    }

    public static Intent getManagerIntent2(){
        return sIntent = new Intent(
                Intent.ACTION_DIAL,
                Uri.parse("tel:" + Uri.encode(
                        FirebaseRemoteConfig
                                .getInstance()
                                .getString(Constants.MANAGER_PHONE_2))));
    }

    public static Intent getChatActivityIntent(Context context, String extraString){
        sIntent = new Intent(context, ChatActivity.class);
        sIntent.putExtra(ChatActivity.EXTRA_TITLE, extraString);
        return sIntent;
    }

    public static Intent getPushActivityIntent(Context context){
        return sIntent = new Intent(context, PushListActivity.class);
    }

    public static Intent getSocialChatIntent(Uri socialUri){
        sIntent = new Intent(Intent.ACTION_VIEW);
        sIntent.setData(socialUri);
        return sIntent;
    }

    public static Intent getFilePickerIntent(){
        sIntent = new Intent();
        sIntent.setType("image/*");
        sIntent.setAction(Intent.ACTION_GET_CONTENT);
        return sIntent;
    }

    public static Intent getMainSiteIntent(String site) {
        sIntent = new Intent(Intent.ACTION_VIEW);
        sIntent.setData(Uri.parse(site));
        return sIntent;
    }
}
