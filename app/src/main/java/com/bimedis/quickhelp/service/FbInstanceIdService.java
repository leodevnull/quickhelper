package com.bimedis.quickhelp.service;

import com.bimedis.quickhelp.utils.FirebaseHelper;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.google.firebase.messaging.FirebaseMessaging;


public class FbInstanceIdService extends FirebaseInstanceIdService {
    private static final String PUSHES = "pushes";

    @Override
    public void onTokenRefresh() {
        FirebaseMessaging.getInstance()
                .subscribeToTopic(PUSHES);
        FirebaseHelper.updateFbToken();
    }
}
