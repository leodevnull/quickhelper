package com.bimedis.quickhelp.service;

import android.app.IntentService;
import android.content.Intent;
import android.support.annotation.Nullable;

import com.bimedis.quickhelp.mvp.model.Message;
import com.bimedis.quickhelp.utils.FirebaseHelper;

public class SendMessageService extends IntentService {
    private static final String TAG = "SendMessageService";
    public static final String ARGS_TOKEN = "ARGS_TOKEN";
    public static final String ARGS_TITLE = "ARGS_TITLE";
    public static final String ARGS_MESSAGE = "ARGS_MESSAGE";


    public SendMessageService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        if (intent != null){
            String title = intent.getStringExtra(ARGS_TITLE);
            String token = intent.getStringExtra(ARGS_TOKEN);
            Message message = intent.getParcelableExtra(ARGS_MESSAGE);
            FirebaseHelper.sendMessage(token, title, message);

        }
    }
}
