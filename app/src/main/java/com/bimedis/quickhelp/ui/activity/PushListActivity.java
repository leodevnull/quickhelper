package com.bimedis.quickhelp.ui.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.bimedis.quickhelp.R;
import com.bimedis.quickhelp.global.SPManager;
import com.bimedis.quickhelp.mvp.presenter.PushListPresenter;
import com.bimedis.quickhelp.mvp.view.PushListView;
import com.bimedis.quickhelp.utils.IntentHelper;
import com.firebase.ui.database.FirebaseRecyclerAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PushListActivity extends BaseActivity implements PushListView{
    @InjectPresenter
    PushListPresenter mPresenter;
    @BindView(R.id.push_toolbar)
    LinearLayout mToolbar;
    @BindView(R.id.push_list_recycler)
    RecyclerView mRecyclerView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_push_list);
        ButterKnife.bind(this);
        mPresenter.onCreate(SPManager.getDeviceToken(this));
    }

    @Override
    public void initToolbar() {
        ((TextView)(mToolbar.findViewById(R.id.toolbar_title))).setText(R.string.push_message);
        ImageView imageView = ((ImageView)(mToolbar.findViewById(R.id.toolbar_logo)));
        imageView.setImageResource(R.drawable.ic_arrow_back_24dp);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    public void setupRecycler(FirebaseRecyclerAdapter adapter) {
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setAdapter(adapter);
    }

    @Override
    public void startChatWithPush(String pushTitle) {
        startActivity(IntentHelper.getChatActivityIntent(this, pushTitle));
    }
}
