package com.bimedis.quickhelp.ui;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.bimedis.quickhelp.R;
import com.labo.kaji.relativepopupwindow.RelativePopupWindow;


public class PopUp extends RelativePopupWindow {
    private ClickInteractions mInteractions;
    private LinearLayout mCameraLayout, mFileLayout;

    public interface ClickInteractions{
        void onCameraClick();
        void onFilePickerClick();
    }

    public PopUp(Context context) {
        View contentView = LayoutInflater.from(context).inflate(R.layout.add_file_popup, null);
        setContentView(contentView);
        setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
        setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
        setFocusable(true);
        setOutsideTouchable(true);
        setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        mCameraLayout = (LinearLayout) contentView.findViewById(R.id.popup_camera);
        mFileLayout = (LinearLayout) contentView.findViewById(R.id.popup_file);
    }

    public void setOnClickListeners(ClickInteractions clickListeners){
        mInteractions = clickListeners;
        initOnClickListeners();
    }

    private void initOnClickListeners(){
        if (mInteractions != null){
            mCameraLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mInteractions.onCameraClick();
                    dismiss();
                }
            });
            mFileLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mInteractions.onFilePickerClick();
                    dismiss();
                }
            });
        }
    }
}
