package com.bimedis.quickhelp.ui.fragment;

import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;

import com.bimedis.quickhelp.R;
import com.bumptech.glide.Glide;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ImagePreviewDialogFragment extends DialogFragment {
    public static final String KEY_FILE_PATH = "KEY_FILE_PATH";
    @BindView(R.id.dialog_image)
    ImageView mImageView;

    public static ImagePreviewDialogFragment newInstance(String filePath) {
        Bundle args = new Bundle();
        args.putString(KEY_FILE_PATH, filePath);
        ImagePreviewDialogFragment fragment = new ImagePreviewDialogFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        String filePath = getArguments().getString(KEY_FILE_PATH);
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.image_preview_dialog, null);
        ButterKnife.bind(this, view);

        final AlertDialog dialog = new AlertDialog.Builder(getContext())
                .setView(view)
                .create();

        dialog.getWindow().setBackgroundDrawable( new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        showPreviewImage(filePath);

        onClickImage(dialog);

        return dialog;
    }

    private void onClickImage(final AlertDialog dialog) {
        mImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    private void showPreviewImage(String filePath) {
        Glide.with(this)
                .load(filePath)
                .into(mImageView);
    }
}
