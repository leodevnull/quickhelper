package com.bimedis.quickhelp.ui.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.content.FileProvider;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.bimedis.quickhelp.BuildConfig;
import com.bimedis.quickhelp.Constants;
import com.bimedis.quickhelp.R;
import com.bimedis.quickhelp.adapter.MessageHolder;
import com.bimedis.quickhelp.global.SPManager;
import com.bimedis.quickhelp.mvp.model.Message;
import com.bimedis.quickhelp.mvp.presenter.ChatPresenter;
import com.bimedis.quickhelp.mvp.view.ChatView;
import com.bimedis.quickhelp.service.SendMessageService;
import com.bimedis.quickhelp.ui.PopUp;
import com.bimedis.quickhelp.ui.fragment.ImagePreviewDialogFragment;
import com.bimedis.quickhelp.utils.IntentHelper;
import com.bumptech.glide.Glide;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.gabrielsamojlo.keyboarddismisser.KeyboardDismisser;
import com.google.firebase.database.Query;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.CompositeMultiplePermissionsListener;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.karumi.dexter.listener.multi.SnackbarOnAnyDeniedMultiplePermissionsListener;
import com.labo.kaji.relativepopupwindow.RelativePopupWindow;

import java.io.File;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class ChatActivity extends BaseActivity implements ChatView{
    private FirebaseRecyclerAdapter<Message, MessageHolder> mAdapter;
    private PopUp mPopupWindow;
    private String mChatChildTitle;
    private ViewGroup mRootView;
    private MultiplePermissionsListener mCompositePermission;
    private MultiplePermissionsListener mPermissionListener = new MultiplePermissionsListener() {
        @Override
        public void onPermissionsChecked(MultiplePermissionsReport report) {
            if (report.areAllPermissionsGranted()){
                mPresenter.onCameraClick();
            }

        }

        @Override
        public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {}
    };


    public static final String EXTRA_TITLE = "EXTRA_TITLE";

    @InjectPresenter
    ChatPresenter mPresenter;
    @BindView(R.id.chat_toolbar)
    LinearLayout mToolbar;
    @BindView(R.id.send_message_text)
    EditText mMessageText;
    @BindView(R.id.chat_recycler)
    RecyclerView mRecyclerView;
    @BindView(R.id.preview_image_layout)
    RelativeLayout mImagePreviewLayout;
    @BindView(R.id.preview_image)
    ImageView mImagePreview;
    @BindView(R.id.preview_cancel)
    ImageView mCancelPreview;

    @OnClick({R.id.send_message_add, R.id.send_message_send})
    public void onClick(View view){
        switch (view.getId()){
            case R.id.send_message_add:
                mPopupWindow.showOnAnchor(
                        view,
                        RelativePopupWindow.VerticalPosition.ABOVE,
                        RelativePopupWindow.HorizontalPosition.CENTER);
                break;
            case R.id.send_message_send:
                String title = mChatChildTitle.equals(Constants.COMMON_CHAT_TITLE) ?
                        getString(R.string.common_chat_title) :
                        mChatChildTitle;
                mPresenter.sendMessage(title, mMessageText.getText().toString());
                break;
        }
    }

    @OnClick(R.id.preview_cancel)
    public void onCancelPreview(){
        mPresenter.onPreviewCancelClick();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        ButterKnife.bind(this);

        mRootView = (ViewGroup) getWindow().getDecorView().getRootView();

        if (!TextUtils.isEmpty(getIntent().getStringExtra(EXTRA_TITLE))){
            mChatChildTitle = getIntent().getStringExtra(EXTRA_TITLE);
        }

        KeyboardDismisser.useWith(this);

        initToolbar();

        initPopUp();

        initPermission();

        mPresenter.onCreate(SPManager.getDeviceToken(this), mChatChildTitle);
    }

    private void initPermission() {
        mCompositePermission = new CompositeMultiplePermissionsListener(mPermissionListener,
                SnackbarOnAnyDeniedMultiplePermissionsListener.Builder
                .with(mRootView, R.string.permission_req)
                .withOpenSettingsButton(R.string.permission_settings)
                .build()
        );
    }

    private void checkPermission(){
        Dexter.withActivity(this)
                .withPermissions(
                        Manifest.permission.CAMERA,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(mCompositePermission)
                .check();
    }

    private void initToolbar() {
        ((TextView)(mToolbar.findViewById(R.id.toolbar_title))).setText(R.string.chat_toolbar_title);
        ImageView imageView = ((ImageView)(mToolbar.findViewById(R.id.toolbar_logo)));
        imageView.setImageResource(R.drawable.ic_arrow_back_24dp);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void initPopUp() {
        mPopupWindow = new PopUp(this);
        mPopupWindow.setOnClickListeners(new PopUp.ClickInteractions() {
            @Override
            public void onCameraClick() {
                checkPermission();
            }

            @Override
            public void onFilePickerClick() {
                mPresenter.onFilePickerClick();
            }
        });
    }

    @Override
    public void setDataToAdapter(Query query){
        mAdapter = new FirebaseRecyclerAdapter<Message, MessageHolder>(
                Message.class, R.layout.chat_message_item, MessageHolder.class, query) {
            @Override
            protected void populateViewHolder(MessageHolder viewHolder, final Message model, int position) {
                viewHolder.onBind(model);
                viewHolder.itemView.findViewById(R.id.message_image).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ImagePreviewDialogFragment dialogFragment = ImagePreviewDialogFragment.newInstance(model.getFile());
                        dialogFragment.show(getSupportFragmentManager(), null);
                    }
                });
            }
        };
        initRecycler();
        scrollRecycler();
    }

    private void initRecycler(){
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setAdapter(mAdapter);
    }

    private void scrollRecycler() {
        mAdapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onItemRangeInserted(int positionStart, int itemCount) {
                super.onItemRangeInserted(positionStart, itemCount);
                mRecyclerView.scrollToPosition(positionStart);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            mPresenter.onActivityResult(requestCode, data != null ? data.getData() : null);
        }else if (resultCode == RESULT_CANCELED){
            mPresenter.onActivityResultCancel();
        }
    }

    @Override
    public void setVisiblePreviewImage(boolean isVisible) {
        mImagePreviewLayout.setVisibility(isVisible ? View.VISIBLE : View.GONE);
    }

    @Override
    public void showImagePreview(String filePath){
        Glide.with(this)
                .load(Uri.parse(filePath))
                .fitCenter()
                .into(mImagePreview);
    }

    @Override
    public void clearInputs() {
        mMessageText.setText(null);
    }

    @Override
    public void startCameraIntent(File imageFile, int requestCamera) {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            if (imageFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this,
                        BuildConfig.APPLICATION_ID,
                        imageFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                List<ResolveInfo> resolvedIntentActivities = getPackageManager()
                        .queryIntentActivities(takePictureIntent, PackageManager.MATCH_DEFAULT_ONLY);

                for (ResolveInfo resolvedIntentInfo : resolvedIntentActivities) {
                    String packageName = resolvedIntentInfo.activityInfo.packageName;
                    grantUriPermission(
                            packageName,
                            photoURI,
                            Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
                }
                startActivityForResult(takePictureIntent, requestCamera);
            }
        }
    }

    @Override
    public void startFilePickerIntent(int requestCode){
        startActivityForResult(
                IntentHelper.getFilePickerIntent(),
                requestCode);
    }

    @Override
    public void startMessageService(String deviceToken, String chatTitle, Message message) {
        Intent intent = new Intent(this, SendMessageService.class);
        intent.putExtra(SendMessageService.ARGS_TOKEN, deviceToken);
        intent.putExtra(SendMessageService.ARGS_TITLE, chatTitle);
        intent.putExtra(SendMessageService.ARGS_MESSAGE, message);
        startService(intent);
    }

    @Override
    public void addImageToGallery(String filePath) {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        Uri contentUri = Uri.fromFile(new File(filePath));
        mediaScanIntent.setData(contentUri);
        this.sendBroadcast(mediaScanIntent);
    }
}
