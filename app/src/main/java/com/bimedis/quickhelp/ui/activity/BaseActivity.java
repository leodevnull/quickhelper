package com.bimedis.quickhelp.ui.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.bimedis.quickhelp.R;
import com.bimedis.quickhelp.mvp.view.BaseView;


public abstract class BaseActivity extends MvpAppCompatActivity implements BaseView{
    private ProgressDialog mDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mDialog = new ProgressDialog(this);
    }

    @Override
    public void showDialog(int title, boolean cancelable){
        mDialog.setTitle(title);
        mDialog.setMessage(getString(R.string.please_wait));
        mDialog.setCancelable(cancelable);
        if (!mDialog.isShowing()){
            mDialog.show();
        }
    }

    @Override
    public void hideDialog(){
        if (mDialog.isShowing()){
            mDialog.dismiss();
        }
    }
}
