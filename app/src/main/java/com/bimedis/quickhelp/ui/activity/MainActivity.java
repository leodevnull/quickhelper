package com.bimedis.quickhelp.ui.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.bimedis.quickhelp.Constants;
import com.bimedis.quickhelp.R;
import com.bimedis.quickhelp.expand.SocialBuilder;
import com.bimedis.quickhelp.expand.SocialGroupAdapter;
import com.bimedis.quickhelp.expand.model.Socials;
import com.bimedis.quickhelp.global.SPManager;
import com.bimedis.quickhelp.mvp.model.Push;
import com.bimedis.quickhelp.mvp.presenter.MainPresenter;
import com.bimedis.quickhelp.mvp.view.MainView;
import com.bimedis.quickhelp.utils.FirebaseHelper;
import com.bimedis.quickhelp.utils.IntentHelper;
import com.bimedis.quickhelp.utils.Utils;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.vicmikhailau.maskededittext.MaskedEditText;
import com.vicmikhailau.maskededittext.MaskedFormatter;
import com.vicmikhailau.maskededittext.MaskedWatcher;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends BaseActivity implements MainView {
    private static final int CHAT_CODE = 0;
    private static final int PUSH_CODE = 1;
    private MaskedWatcher mWatcher;
    private boolean isDoubleTap = false;
    @InjectPresenter
    MainPresenter mPresenter;
    @BindView(R.id.main_manager_1)
    View mViewManager1;
    @BindView(R.id.main_manager_2)
    View mViewManager2;
    @BindView(R.id.main_site)
    View mSiteView;
    @BindView(R.id.main_expand_recycler)
    RecyclerView mExpandRecycler;
    @BindView(R.id.main_chat)
    View mViewChat;
    @BindView(R.id.phone_number)
    MaskedEditText mPhoneNumber;
    @BindView(R.id.main_push)
    View mViewPush;
    @BindView(R.id.main_bottom_social)
    View mViewBottomSocial;

    @OnClick({
            R.id.main_manager_1,
            R.id.main_manager_2,
            R.id.main_site,
            R.id.main_chat,
            R.id.main_push,
            R.id.request_call,
            R.id.request_call_lang
    })
    public void onClickRedButton(View view){
        switch (view.getId()){
            case R.id.main_manager_1:
                startActivity(IntentHelper.getManagerIntent1());
                break;
            case R.id.main_manager_2:
                startActivity(IntentHelper.getManagerIntent2());
                break;
            case R.id.main_site:
                startActivity(IntentHelper.getMainSiteIntent(
                        FirebaseRemoteConfig.getInstance().getString(Constants.MAIN_SITE)));
                break;
            case R.id.main_chat:
                if (checkInternet()) {
                    mPresenter.startIntent(CHAT_CODE);
                }
                break;
            case R.id.main_push:
                if (checkInternet()) {
                    mPresenter.startIntent(PUSH_CODE);
                }
                break;
            case R.id.request_call:
                if (checkInternet()) {
                    String phone = mPhoneNumber.getText().toString();
                    String hint = mPhoneNumber.getHint().toString();
                    String simLanguage = SPManager.getSimLanguage(this);
                    hideSoftKeyboard();
                    mPresenter.requestCall(simLanguage, phone, hint);
                }
                break;
            case R.id.request_call_lang:
                new AlertDialog.Builder(this).setTitle(R.string.choice_lang)
                        .setItems(getResources().getStringArray(R.array.language), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                int lang = SPManager.getUserPhoneLang(getApplicationContext());
                                if (which != lang){
                                    mPresenter.updateLanguage(which);
                                    mPhoneNumber.setText("");
                                    SPManager.setUserPhone(getApplicationContext(), "");
                                    SPManager.setUserPhoneLang(getApplicationContext(), which);
                                }
                            }
                        }).show();
                break;
        }
    }

    @OnClick({
            R.id.social_fb,
            R.id.social_vk,
            R.id.social_ok,
            R.id.social_twitter,
            R.id.social_google
            })
    public void onSocialClick(View view){
        Intent socialIntent = null;
        switch (view.getId()){
            case R.id.social_fb:
                socialIntent = IntentHelper.getFbLinkIntent(this);
                break;
            case R.id.social_vk:
                socialIntent = IntentHelper.getVkIntent();
                break;
            case R.id.social_ok:
                socialIntent = IntentHelper.getOkIntent();
                break;
            case R.id.social_twitter:
                socialIntent = IntentHelper.getTwitIntent();
                break;
            case R.id.social_google:
                socialIntent = IntentHelper.getGoogleIntent();
                break;
        }
        if (socialIntent != null)
            startActivity(socialIntent);
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (checkInternet()) {
            mPresenter.checkUserOnStart();
        }
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        mPresenter.initRemoteConfig();

        receivePush();

        initIncludeLayout();

        initMaskedEditText();

        initExpandRecycler();
    }

    private void receivePush() {
        if (getIntent().getExtras() != null && getIntent().getStringExtra(Constants.PUSH_UID) != null){
            if (FirebaseAuth.getInstance().getCurrentUser() != null) {
                int uid = Integer.parseInt(getIntent().getStringExtra(Constants.PUSH_UID));
                FirebaseHelper.getPushTitleById(uid)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        for (DataSnapshot x : dataSnapshot.getChildren()){
                            String title = x.getValue(Push.class).getTitle();
                            if (!TextUtils.isEmpty(title)){
                                startActivity(IntentHelper.getChatActivityIntent(MainActivity.this, title));
                            }
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }
        }
    }

    @Override
    public void initManagersTitle(String title1, String title2){
        ((TextView)mViewManager1.findViewById(R.id.red_button_text)).setText(title1);
        ((TextView)mViewManager2.findViewById(R.id.red_button_text)).setText(title2);
    }

    private void initIncludeLayout() {
        ((TextView)mViewChat.findViewById(R.id.red_button_text)).setText(R.string.chat);
        ((ImageView)mViewChat.findViewById(R.id.red_button_image)).setImageResource(R.drawable.ic_bubbles);

        ((TextView)mViewPush.findViewById(R.id.red_button_text)).setText(R.string.push_message);
        ((ImageView)mViewPush.findViewById(R.id.red_button_image)).setImageResource(R.drawable.ic_notifications_24dp);
    }

    private void initMaskedEditText(){
        int lang = SPManager.getUserPhoneLang(this);
        if (lang == -1){
            lang = Utils.getLanguageCodeBySim(this);
            SPManager.setUserPhoneLang(this, lang);
        }
        mPresenter.updateLanguage(lang);

        if (!TextUtils.isEmpty(SPManager.getUserPhone(this))){
            mPhoneNumber.setText(SPManager.getUserPhone(this));
        }
    }

    private void initExpandRecycler() {
        RecyclerView.ItemAnimator animator = mExpandRecycler.getItemAnimator();
        if (animator instanceof DefaultItemAnimator) {
            ((DefaultItemAnimator) animator).setSupportsChangeAnimations(false);
        }
        SocialGroupAdapter socialGroupAdapter = new SocialGroupAdapter(
                SocialBuilder.getSocialGroup(
                        this,
                        getString(R.string.contact_with_us)), new SocialGroupAdapter.OnSocialClickListener() {
            @Override
            public void onItemClick(Socials socials) {
                startActivity(IntentHelper.getSocialChatIntent(socials.getSocialUri()));
            }
        });
        mExpandRecycler.setLayoutManager(new LinearLayoutManager(this));
        mExpandRecycler.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        mExpandRecycler.setAdapter(socialGroupAdapter);
    }

    private void hideSoftKeyboard() {
        if(getCurrentFocus()!=null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }

    private boolean checkInternet(){
        if (!Utils.isOnline(this)){
            Toast.makeText(this, R.string.no_internet_connection, Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

    @Override
    public void saveNewTokens(String token, String device_token) {
        SPManager.setUserToken(this, token);
        SPManager.setDeviceToken(this, device_token);
    }

    @Override
    public void startIntent(int intentCode) {
        switch (intentCode){
            case CHAT_CODE:
                startActivity(IntentHelper.getChatActivityIntent(this, Constants.COMMON_CHAT_TITLE));
                break;
            case PUSH_CODE:
                startActivity(IntentHelper.getPushActivityIntent(this));
                break;
        }
    }

    @Override
    public void saveUserPhone(String phone) {
        if (!SPManager.getUserPhone(this).equals(phone)){
            SPManager.setUserPhone(this, phone);
        }
    }

    @Override
    public void showMessage(int messageIdRes) {
        Toast.makeText(this, messageIdRes, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void updateMaskedEditText(MaskedFormatter formatter, String hint) {
        if (mWatcher != null){
            mPhoneNumber.removeTextChangedListener(mWatcher);
        }
        mWatcher = new MaskedWatcher(formatter, mPhoneNumber);
        mPhoneNumber.setHint(hint);
        mPhoneNumber.addTextChangedListener(mWatcher);
    }

    @Override
    public void onBackPressed() {
        if (isDoubleTap){
            super.onBackPressed();
            return;
        }
        isDoubleTap = true;
        Toast.makeText(this, R.string.double_tap_toast, Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                isDoubleTap=false;
            }
        }, 2000);
    }
}
