package com.bimedis.quickhelp;



public class Constants {
    public static final String PUSH_UID = "PUSH_UID";
    public static final String SKYPE_PACKAGE = "com.skype.raider";
    public static final String VIBER_PACKAGE = "com.viber.voip";
    public static final String FB_MESSENGER_PACKAGE = "com.facebook.orca";
    public static final String FB_PACKAGE = "com.facebook.katana";
    public static final String TELEGRAM_PACKAGE = "org.telegram.messenger";
    public static final String COMMON_CHAT_TITLE = "common";

    public static final String MANAGER_LABEL_EN_1 = "MANAGER_LABEL_EN_1";
    public static final String MANAGER_LABEL_EN_2 = "MANAGER_LABEL_EN_2";
    public static final String MANAGER_LABEL_RU_1 = "MANAGER_LABEL_RU_1";
    public static final String MANAGER_LABEL_RU_2 = "MANAGER_LABEL_RU_2";
    public static final String MANAGER_LABEL_UA_1 = "MANAGER_LABEL_UA_1";
    public static final String MANAGER_LABEL_UA_2 = "MANAGER_LABEL_UA_2";
    public static final String MANAGER_PHONE_1 = "MANAGER_PHONE_1";
    public static final String MANAGER_PHONE_2 = "MANAGER_PHONE_2";
    public static final String MAIN_SITE = "MAIN_SITE";
    public static final String SKYPE_CHAT = "SKYPE_CHAT";
    public static final String VIBER_CHAT = "VIBER_CHAT";
    public static final String VK_CHAT = "VK_CHAT";
    public static final String TELTGRAM_CHAT = "TELTGRAM_CHAT";
    public static final String LINKEDIN_CHAT = "LINKEDIN_CHAT";
    public static final String GOOGLE_CHAT = "GOOGLE_CHAT";
    public static final String FB_CHAT = "FB_CHAT";
    public static final String OK_CHAT = "OK_CHAT";

    public static final String GOOGLE_LINK = "GOOGLE_LINK";
    public static final String FB_LINK = "FB_LINK";
    public static final String OK_LINK = "OK_LINK";
    public static final String VK_LINK = "VK_LINK";
    public static final String TWIT_LINK = "TWIT_LINK";

    public static final int LANG_RU = 0;
    public static final int LANG_UA = 1;
    public static final String ISO_LANG_RU = "ru";
    public static final String ISO_LANG_UA = "uk";

    public static final String MASK_RU = "+7(###)###-##-##";
    public static final String MASK_UA = "+380(##)###-##-##";
    public static final String HINT_RU = "+7(123)456–78–90";
    public static final String HINT_UA = "+380(12)345-67-89";

}
