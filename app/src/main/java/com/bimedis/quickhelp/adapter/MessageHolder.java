package com.bimedis.quickhelp.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bimedis.quickhelp.R;
import com.bimedis.quickhelp.mvp.model.Message;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MessageHolder extends RecyclerView.ViewHolder {
    private Context mContext;
    @BindView(R.id.message_container)
    LinearLayout mLayout;
    @BindView(R.id.message_title)
    TextView mTitle;
    @BindView(R.id.message_text)
    TextView mText;
    @BindView(R.id.message_image)
    ImageView mImage;


    public MessageHolder(View itemView) {
        super(itemView);
        mContext = itemView.getContext();
        ButterKnife.bind(this, itemView);
    }

    public void onBind(Message message){
        int textColor = message.getType() == 0 ? Color.WHITE : Color.BLACK;
        int bgDrawable = message.getType() == 0 ? R.drawable.red_chat_bubble : R.drawable.white_chat_bubble;
        int gravity = message.getType() == 0 ? Gravity.END : Gravity.START;
        mLayout.setGravity(gravity);
        mLayout.findViewById(R.id.message_inner_layout).setBackgroundResource(bgDrawable);
        mTitle.setText(message.getTitle());
        mTitle.setTextColor(textColor);
        mText.setText(message.getText());
        mText.setTextColor(textColor);
        if (!TextUtils.isEmpty(message.getFile())){
            Glide.with(mContext)
                    .load(message.getFile())
                    .asBitmap()
                    .crossFade(1000)
                    .override(500,400)
                    .fitCenter()
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .into(mImage);
        }else {
            Glide.clear(mImage);
        }
    }
}
