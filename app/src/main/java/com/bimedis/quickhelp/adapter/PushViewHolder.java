package com.bimedis.quickhelp.adapter;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bimedis.quickhelp.R;
import com.bimedis.quickhelp.mvp.model.Push;

import butterknife.BindView;
import butterknife.ButterKnife;


public class PushViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.push_item_like)
    ImageView mImageLike;
    @BindView(R.id.push_item_title)
    TextView mTitle;
    @BindView(R.id.push_item_button)
    View mChat;

    public PushViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public void bind(Push push){
        mTitle.setText(push.getTitle());
        mImageLike.setImageResource(R.drawable.ic_like);
        mImageLike.setColorFilter(push.isLike() ? Color.RED : Color.GRAY);
    }

    public void toggleLike(boolean isLike){
        mImageLike.setColorFilter(isLike ? Color.RED : Color.GRAY);
    }
}
