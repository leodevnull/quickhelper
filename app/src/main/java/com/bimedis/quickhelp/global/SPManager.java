package com.bimedis.quickhelp.global;


import android.content.Context;
import android.content.SharedPreferences;

public class SPManager {
    private static final String KEY_USER_TOKEN = "KEY_USER_TOKEN";
    private static final String KEY_DEVICE_TOKEN = "KEY_DEVICE_TOKEN";
    private static final String KEY_USER_PHONE = "KEY_USER_PHONE";
    private static final String KEY_USER_PHONE_LANG = "KEY_USER_PHONE_LANG";
    private static final String KEY_SIM_LANGUAGE = "KEY_SIM_LANGUAGE";
    private static final String PREFS_NAME = "PREFS_NAME";

    private static SharedPreferences mSharedPreferences;

    private static SharedPreferences getPref(Context context){
       return context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
    }

    public static void setUserToken(Context context, String token){
        if (context == null) return;
        mSharedPreferences = getPref(context);
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(KEY_USER_TOKEN, token);
        editor.apply();
    }

    public static void setDeviceToken(Context context, String token){
        if (context == null) return;
        mSharedPreferences = getPref(context);
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(KEY_DEVICE_TOKEN, token);
        editor.apply();
    }

    public static void setUserPhone(Context context, String phone){
        if (context == null) return;
        mSharedPreferences = getPref(context);
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(KEY_USER_PHONE, phone);
        editor.apply();
    }

    public static String getUserPhone(Context context){
        mSharedPreferences = getPref(context);
        return mSharedPreferences.getString(KEY_USER_PHONE, "");
    }

    public static void setUserPhoneLang(Context context, int langCode){
        if (context == null) return;
        mSharedPreferences = getPref(context);
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putInt(KEY_USER_PHONE_LANG, langCode);
        editor.apply();
    }

    public static int getUserPhoneLang(Context context){
        mSharedPreferences = getPref(context);
        return mSharedPreferences.getInt(KEY_USER_PHONE_LANG, -1);
    }

    public static String getUserToken(Context context){
        mSharedPreferences = getPref(context);
        return mSharedPreferences.getString(KEY_USER_TOKEN, "");
    }

    public static String getDeviceToken(Context context){
        mSharedPreferences = getPref(context);
        return mSharedPreferences.getString(KEY_DEVICE_TOKEN, "");
    }

    public static void setSimLanguage(Context context, String countryCode) {
        if (context == null) return;
        mSharedPreferences = getPref(context);
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(KEY_SIM_LANGUAGE, countryCode);
        editor.apply();
    }

    public static String getSimLanguage(Context context){
        mSharedPreferences = getPref(context);
        return mSharedPreferences.getString(KEY_SIM_LANGUAGE, "");
    }

}
