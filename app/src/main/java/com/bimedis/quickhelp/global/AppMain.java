package com.bimedis.quickhelp.global;

import android.app.Application;
import android.support.annotation.NonNull;

import com.bimedis.quickhelp.BuildConfig;
import com.bimedis.quickhelp.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;


public class AppMain extends Application {
    
    @Override
    public void onCreate() {
        super.onCreate();
        FirebaseDatabase.getInstance().setPersistenceEnabled(true);
    }

}
